

## WYSIWYG POC

What you see is what you get WYSIWYG, will present different options to implement in a react application.

### Not FREE

#### Ckeditor

This look really complete but is not FREE after 5 simultanues users.
Open["https://ckeditor.com/pricing/) https://ckeditor.com/pricing/
Has support for React and looks easy to implement.

Open["https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/react.html) https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/react.html

Here you can find a live DEMO:

Open["https://jpuri.github.io/react-draft-wysiwyg/#/) https://jpuri.github.io/react-draft-wysiwyg/#/




## FREE or Partial FREE

#### React Draft Wysiwyg

Open["https://github.com/jpuri/react-draft-wysiwyg) https://github.com/jpuri/react-draft-wysiwyg

Live DEMO:

Open["https://jpuri.github.io/react-draft-wysiwyg/#/demo) https://jpuri.github.io/react-draft-wysiwyg/#/demo

#### tiny.cloud

Is Free but have some Pricing for premium pluggings.

Open["https://github.com/tinymce/) https://github.com/tinymce/

Easy to integrate with React:

Open["https://www.tiny.cloud/docs/integrations/react/#tinymcereactintegrationquickstartguide) https://www.tiny.cloud/docs/integrations/react/#tinymcereactintegrationquickstartguide

Live DEMO

Open["https://tinymce.github.io/tinymce-react/?path=/story/tinymce-react--iframe-editor) https://tinymce.github.io/tinymce-react/?path=/story/tinymce-react--iframe-editor

#### draftjs.org

Completly FREE but is looks incomplete.

Open["https://draftjs.org/) https://draftjs.org/

But have a good React documentation.

Open["https://draftjs.org/docs/getting-started) https://draftjs.org/docs/getting-started

#### React JS Jodit WYSIWYG

This is the most complete and FREE

Open["https://xdsoft.net/jodit/) https://xdsoft.net/jodit/

Cool big DEMO

Open["https://xdsoft.net/jodit/play.html) https://xdsoft.net/jodit/play.html

Support for REACT

Open["https://xdsoft.net/jodit/examples/intergration/react-jodit.html) https://xdsoft.net/jodit/examples/intergration/react-jodit.html

#### Alloy Editor 

Complete free and DEMO but i think will need more functionalities.

Open["https://alloyeditor.com/) https://alloyeditor.com/



### Conclusion 
For me the most competitive ones are Ckeditor and JS Jodit and putting one beside the other one Jodit is completely FREE and for me looks more complete.

I try to create a Live DEMO for JS Jodit but the react component they have is having some issues, and from the issues section various people report different issues and they recommend better use the Vanilla JS option. So I create a demo with Jodit react vs Jodit Vanilla in react and an extra for Ckeditor.

## DEMO in LIVE

http://audelio.dev/poceditor