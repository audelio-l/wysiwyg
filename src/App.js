import React from 'react'
import './App.css'
import Editor from './editor'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'

function App () {
  return (
    <div className='App'>
      <header className='App-header'>
        <h1>WYSIWYG DEMO </h1>
        <h2>Ckeditor</h2>
        <CKEditor
          editor={ClassicEditor}
          data='<p>Hello from CKEditor 5!</p>'
          onInit={editor => {
            // You can store the "editor" and use when it is needed.
            console.log('Editor is ready to use!', editor)
          }}
          onChange={(event, editor) => {
            const data = editor.getData()
            console.log({ event, editor, data })
          }}
          onBlur={(event, editor) => {
            console.log('Blur.', editor)
          }}
          onFocus={(event, editor) => {
            console.log('Focus.', editor)
          }}
        />
        <h2>JODIT REACT</h2>
        <Editor />
        <h2>JODIT Vanilla</h2>
        <div id='editor' />
        <div style={{ 'padding-bottom': '300px' }} />
      </header>
    </div>
  )
}

export default App
